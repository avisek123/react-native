
import React, { Component } from 'react'
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  StatusBar
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SignIn from './SignIn';
import SignUp from './SignUp'; 
const Stack = createStackNavigator(); 
const myOptions = {
  headerTintColor: 'white',
  headerStyle: {
    backgroundColor: '#020a75'
  }

}
const App = () => {
  return (
   
    
    <View style={styles.container}>
      <NavigationContainer> 
        <Stack.Navigator>
          <Stack.Screen name="Sign Up" component={SignUp}
            options={{
              ...myOptions,
              title: 'Sign Up'
            }}
          />
          <Stack.Screen name="Sign In" component={SignIn}
            options={{
              ...myOptions,
              title: 'Sign In'
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>

    
      </View>
   
  );
}
export default App;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e0e0e0',

// marginTop: 20

  },

})
