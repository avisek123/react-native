import React, { useState } from 'react';
import { View, Text, StyleSheet, Modal } from 'react-native';
import { TextInput, Button } from 'react-native-paper';

const SignUp = ({navigation}) => {
    
    const [email, setEmail] = useState('');
    const [password, setPassowrd] = useState('')

    return (
        <View >
            <View style={styles.root}>
               
                <TextInput
                    label="Email"
                    style={styles.input}
                    value={email}
                    theme={theme}
                    mode='outlined'
                    onChangeText={text => setEmail(text)}
                />
               
                <TextInput
                    label="Password"
                    style={styles.input}
                    value={password}
                    theme={theme}
                    keyboardType='number-pad'
                    mode='outlined'
                    onChangeText={text => setPassowrd(text)}
                /> 




                <View style={{ alignItems: 'center' }}>

                    <Button icon="login" 
                        style={{ marginTop: 15 }}
                        mode="contained"
                        theme={theme}
                        onPress={() => setModal(true)}> 
                        Sign In
  </Button>
                    <Text style={{ marginTop: 15, color: '#0031c4' }}> Don't have an account ?</Text>
                    <Button icon="content-save"
                        style={{ marginTop: 15, backgroundColor: '#020a75' }}
                        mode="contained"
                        theme={theme}
                        onPress={() => navigation.goBack('Sign Up')}>
                        Sign Up
  </Button>
                </View>


            </View>
        </View>
    );
}
const theme = {
    colors: {
        primary: '#f50530',

    }
}

export default SignUp;
const styles = StyleSheet.create({
    root: {
        flex: 1,
        //backgroundColor:'#e4e5eb'

    },
    input: {
        margin: 4
    },



})